#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <set>
#include <map>
#include <tuple>

using namespace std;


class GRAPH
{
public:
	GRAPH();
	~GRAPH();
	void readGraph(string fileName);
	void addEdge(int from, int to, int weight);
	void removeEdge(int from, int to);
	int changeEdge(int from, int to, int newWeight);
	void transformToAdjList();
	void transformToAdjMatrix();
	void transformToListOfEdges();
	void writeGraph(string fileName);
	

private:

	//����������
	void getAdjList();
	void getAdjMatrix();
	void getListOfEdges();

	//�����������������
	void transformToAdjMatrixFromAdjList();
	void transformToAdjMatrixFromListOfEdges();

	void transformToAdjListFromMatrix();
	void transformToAdjListFromListOfEdges();

	void transformToListOfEdgeFromMatrix();
	void transformToListOfEdgeFromAdjList();

	bool getBoolFromStr(char s);
	char typeOfGraf;
	int N, M;
	bool W, R;
	char ch;
	ifstream textFile;
	ofstream outTestFile;
	//������� ���������
	vector <vector <int>> matrixGraf;

	//������ ���������
		//��� ������������� �����
	vector<set<pair<int, int>>> adjListOfNotWeighted;
		//��� ����������� �����
	vector<set<tuple<int, int, int>>> adjListOfWeighted;

	//������ �����
		//��� ������������� �����
	vector<set<pair<int, int>>> ListOfNotWeighted;
		//��� ����������� �����
	vector<set<tuple<int, int, int>>> ListOfWeighted;
};

GRAPH::GRAPH()
{
}

GRAPH::~GRAPH()
{
}

bool GRAPH::getBoolFromStr(char s)
{
	if (s == '1')
	{
		return true;
	}
	else
	{
		return false;
	}
}

void GRAPH::readGraph(string fileName)
{
	
	textFile.open(fileName, ios::in);
	textFile >> typeOfGraf;
	switch (typeOfGraf)
	{
	case 'C':
	{
				getAdjMatrix();
				break;
	}
	case 'L':
	{
				getAdjList();
				break;
	}
	case 'E':
	{
				getListOfEdges();
				break;
	}
	default:
		break;
	}
}

void GRAPH::getAdjMatrix()
{
	textFile >> N;
	textFile >> ch;
	R = getBoolFromStr(ch);
	textFile >> ch;
	W = getBoolFromStr(ch);
	matrixGraf.resize(N);
	M = 0;
	for (int i = 0; i < N; i++)
	{
		matrixGraf[i].resize(N);
		for (int j = 0; j < N; j++)
		{
			textFile >> matrixGraf[i][j];
			if (matrixGraf[i][j] != 0)
			{
				M++;
			}
		}
	}

	if (R != true)
	{
		M =M/ 2;
	}

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << matrixGraf[i][j] << " ";
		}
		std::cout << endl;
	}
}

void GRAPH::getListOfEdges()
{
	int A_i, B_i, W_i,ID;
	ID = 1;
	textFile >> ch;
	N = atoi(&ch);
	textFile >> ch;
	M = atoi(&ch);
	textFile >> ch;
	R = getBoolFromStr(ch);
	textFile >> ch;
	W = getBoolFromStr(ch);
	
	if (W == true)
	{
		ListOfWeighted.resize(N + 1);
		while (!textFile.eof())
		{
			textFile >> A_i;
			if (A_i == ' ')
			{
				break;
			}
			textFile >> B_i;
			textFile >> W_i;
			if (R == 1)
			{
				ListOfWeighted[A_i].insert(make_tuple(B_i, W_i, ID));
			}
			else
			{
				ListOfWeighted[A_i].insert(make_tuple(B_i, W_i, ID));
				ListOfWeighted[B_i].insert(make_tuple(A_i, W_i, ID));
			}
			ID++;
		}
	}
	else
	{
		ListOfNotWeighted.resize(N + 1);
		while (!textFile.eof())
		{
			textFile >> A_i;
			textFile >> B_i;
			if (R == 1)
			{				
				ListOfNotWeighted[A_i].insert(make_pair(B_i, ID));
			}
			else
			{
				ListOfNotWeighted[A_i].insert(make_pair(B_i, ID));
				ListOfNotWeighted[B_i].insert(make_pair(A_i, ID));
			}
			ID++;
		}
	}
}

void GRAPH::getAdjList()
{
	int B_i, W_i,ID;
	bool f;
	f = false;
	B_i = 0;
	W_i = 0;
	ID = 1;
	textFile >> N;
	textFile >> ch;
	R = getBoolFromStr(ch);
	textFile >> ch;
	W = getBoolFromStr(ch);
	
	if (W == true)
	{
		adjListOfWeighted.resize(N + 1);
		for (int i = 0; i < N; i++)
		{
			while (!f)
			{
				textFile.get(ch);
				if ((ch == '\n') && (f == false))
				{
					break;
				}
				while (ch != ' ')
				{
					B_i *= 10;
					B_i += atoi(&ch);
					textFile.get(ch);
				}
				textFile.get(ch);
				while (ch != ' ' && ch != '\n')
				{
					W_i *= 10;
					W_i += atoi(&ch);
					textFile.get(ch);
					if (ch == '\n')
					{
						f = true;
					}
				}
				
				std::cout << B_i << endl;
				std::cout << W_i << endl;
				if (R == 1)
				{
					adjListOfWeighted[i + 1].insert(make_tuple(B_i, W_i, ID));
					M++;
				}
				else
				{
					adjListOfWeighted[i + 1].insert(make_tuple(B_i, W_i, ID));
					ID++;
					adjListOfWeighted[B_i].insert(make_tuple(i + 1, W_i, ID));
					M += 2;
				}
				ID++;
				B_i = 0;
				W_i = 0;
			}		
			f = false;
		}
	}
	else
	{
		adjListOfNotWeighted.resize(N + 1);
		textFile.get(ch);
		for (int i = 0; i < N; i++)
		{
			while (!f)
			{
				textFile.get(ch);
				while (ch != ' ' && ch!='\n')
				{
					B_i *= 10;
					B_i += atoi(&ch);
					textFile.get(ch);
					if (ch == '\n')
					{
						f = true;
					}
				}
				if ((ch == '\n') && (f == false))
				{
					break;
				}
				std::cout << B_i << " ";
				if (R == 1)
				{
					adjListOfNotWeighted[i+1].insert(make_pair(B_i, ID));
					M++;
				}
				else
				{
					adjListOfNotWeighted[i+1].insert(make_pair(B_i, ID));
					ID++;
					adjListOfNotWeighted[B_i].insert(make_pair(i+1, ID));
					M += 2;
				}
				ID++;
				B_i = 0;
			}
			f = false;
			std::cout << endl;
		}
	}

}

void GRAPH::transformToAdjMatrix()
{
	switch (typeOfGraf)
	{
	case 'L':
	{
				transformToAdjMatrixFromAdjList();
				break;
	}
	case 'E':
	{
				transformToAdjMatrixFromListOfEdges();
				break;
	}
	}
}

void GRAPH::transformToAdjMatrixFromAdjList()
{
	typeOfGraf = 'C';
	matrixGraf.resize(N);
	for (int i = 0; i < N; i++)
	{
		matrixGraf[i].resize(N);
		for (int j = 0; j < N; j++)
		{
			matrixGraf[i][j] = 0;
		}
	}
	if (W == 0)
	{
		for (int i = 1; i < N+1; i++)
		{
			for (auto j = adjListOfNotWeighted[i].begin(); j != adjListOfNotWeighted[i].end(); j++)
			{
				matrixGraf[i-1][j->first-1] = 1;
				if (R == 0)
				{
					matrixGraf[j->first-1][i-1] = 1;
				}
			}
		}
	}
	else
	{
		for (int i = 1; i < N + 1; i++)
		{
			for (auto j = adjListOfWeighted[i].begin(); j != adjListOfWeighted[i].end(); j++)
			{
				matrixGraf[i - 1][get<0>(*j) - 1] = get<1>(*j);
				if (R == 0)
				{
					matrixGraf[get<0>(*j) - 1][i - 1] = get<1>(*j);
				}
			}
		}
	}
}

void GRAPH::transformToAdjMatrixFromListOfEdges()
{
	typeOfGraf = 'C';
	matrixGraf.resize(N);
	for (int i = 0; i < N; i++)
	{
		matrixGraf[i].resize(N);
		for (int j = 0; j < N; j++)
		{
			matrixGraf[i][j] = 0;
		}
	}
	if (W == 0)
	{
		for (int i = 1; i < N + 1; i++)
		{
			for (auto j = ListOfNotWeighted[i].begin(); j != ListOfNotWeighted[i].end(); j++)
			{
				matrixGraf[i - 1][j->first - 1] = 1;
				if (R == 0)
				{
					matrixGraf[j->first - 1][i - 1] = 1;
				}
			}
		}
	}
	else
	{
		for (int i = 1; i < N + 1; i++)
		{
			for (auto j = ListOfWeighted[i].begin(); j != ListOfWeighted[i].end(); j++)
			{
				matrixGraf[i - 1][get<0>(*j) - 1] = get<1>(*j);
				if (R == 0)
				{
					matrixGraf[get<0>(*j) - 1][i - 1] = get<1>(*j);
				}
			}
		}
	}
}

void GRAPH::transformToAdjList()
{
	switch (typeOfGraf)
	{
	case 'C':
	{
				transformToAdjListFromMatrix();
				break;
	}
	case 'E':
	{
				transformToAdjListFromListOfEdges();
				break;
	}
	}
}

void GRAPH::transformToAdjListFromMatrix()
{
	typeOfGraf = 'L';
	int ID = 0;
	if (W == true)
	{
		adjListOfWeighted.resize(N + 1);
		if (R != true)
		{
			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					if (matrixGraf[i][j] != 0)
					{
						adjListOfWeighted[i + 1].insert(make_tuple(j + 1, matrixGraf[i][j], ID));
						ID++;
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < N; i++)
			{
				for (int j = i; j < N; j++)
				{
					if (matrixGraf[i][j] != 0)
					{
						adjListOfWeighted[i + 1].insert(make_tuple(j + 1, matrixGraf[i][j], ID));
						ID++;
					}
				}
			}
		}
	}
	else
	{
		adjListOfNotWeighted.resize(N + 1);
		if (R != true)
		{
			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					if (matrixGraf[i][j] != 0)
					{
						adjListOfNotWeighted[i + 1].insert(make_pair(j + 1, ID));
						ID++;
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < N; i++)
			{
				for (int j = i; j < N; j++)
				{
					if (matrixGraf[i][j] != 0)
					{
						adjListOfNotWeighted[i + 1].insert(make_pair(j + 1, ID));
						ID++;
					}
				}
			}
		}

	}
}

void GRAPH::transformToAdjListFromListOfEdges()
{
	bool f = true;
	typeOfGraf = 'L';
	if (W == 1)
	{
		adjListOfWeighted.resize(N + 1);
		for (int i = 1; i < N + 1; i++)
		{
			for (auto j = ListOfWeighted[i].begin(); j != ListOfWeighted[i].end(); j++)
			{
				if (R != true)
				{
					adjListOfWeighted[i].insert(make_tuple(get<0>(*j), get<1>(*j), get<2>(*j)));
				}
				else
				{
					for (auto z = adjListOfWeighted[get<0>(*j)].begin(); z != adjListOfWeighted[get<0>(*j)].end(); z++)
					{
						if (get<0>(*z) == i)
						{
							f = false;
							break;
						}
					}
					if (f)
					{
						adjListOfWeighted[i].insert(make_tuple(get<0>(*j), get<1>(*j), get<2>(*j)));
					}
					f = true;
				}
			}
		}
	}
	else
	{
		adjListOfNotWeighted.resize(N + 1);
		for (int i = 1; i < N + 1; i++)
		{
			for (auto j = ListOfNotWeighted[i].begin(); j != ListOfNotWeighted[i].end(); j++)
			{
				if (R != true)
				{
					adjListOfNotWeighted[i].insert(make_pair(j->first, j->second));
				}
				else
				{
					for (auto z = adjListOfNotWeighted[j->first].begin(); z != adjListOfNotWeighted[j->first].end(); z++)
					{
						if (z->first == i)
						{
							f = false;
							break;
						}
					}
					if (f)
					{
						adjListOfNotWeighted[i].insert(make_pair(j->first, j->second));
					}
					f = true;
				}
			}
		}
	}
}

void GRAPH::transformToListOfEdges()
{
	switch (typeOfGraf)
	{
	case 'C':
	{
				transformToListOfEdgeFromMatrix();
				break;
	}
	case 'L':
	{
				transformToListOfEdgeFromAdjList();
				break;
	}
	}
}

void GRAPH::transformToListOfEdgeFromMatrix()
{
	typeOfGraf = 'E';
	int ID = 0;
	if (W == true)
	{
		ListOfWeighted.resize(N + 1);
		if (R == true)
		{
			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					if (matrixGraf[i][j] != 0)
					{
						ListOfWeighted[i + 1].insert(make_tuple(j + 1, matrixGraf[i][j], ID));
						ID++;
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < N; i++)
			{
				for (int j = i; j < N; j++)
				{
					if (matrixGraf[i][j] != 0)
					{
						ListOfWeighted[i + 1].insert(make_tuple(j + 1, matrixGraf[i][j], ID));
						ID++;
					}
				}
			}
		}
	}
	else
	{
		ListOfNotWeighted.resize(N + 1);
		if (R == true)
		{
			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					if (matrixGraf[i][j] != 0)
					{
						ListOfNotWeighted[i + 1].insert(make_pair(j + 1, ID));
						ID++;
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < N; i++)
			{
				for (int j = i; j < N; j++)
				{
					if (matrixGraf[i][j] != 0)
					{
						ListOfNotWeighted[i + 1].insert(make_pair(j + 1, ID));
						ID++;
					}
				}
			}
		}
	}
}

void GRAPH::transformToListOfEdgeFromAdjList()
{
	bool f = true;
	typeOfGraf = 'E';
	if (W == 1)
	{
		ListOfWeighted.resize(N + 1);
		for (int i = 1; i < N + 1; i++)
		{
			for (auto j = adjListOfWeighted[i].begin(); j != adjListOfWeighted[i].end(); j++)
			{
				if (R != true)
				{
					ListOfWeighted[i].insert(make_tuple(get<0>(*j), get<1>(*j), get<2>(*j))); 
				}
				else
				{
					for (auto z = ListOfWeighted[get<0>(*j)].begin(); z != ListOfWeighted[get<0>(*j)].end(); z++)
					{
						if (get<0>(*z) == i)
						{
							f = false;
							break;
						}
					}
					if (f)
					{
						ListOfWeighted[i].insert(make_tuple(get<0>(*j), get<1>(*j), get<2>(*j)));
					}
					f = true;
				}
			}
		}
	}
	else
	{
		ListOfNotWeighted.resize(N + 1);
		for (int i = 1; i < N + 1; i++)
		{
			for (auto j = adjListOfNotWeighted[i].begin(); j != adjListOfNotWeighted[i].end(); j++)
			{
				if (R != true)
				{
					ListOfNotWeighted[i].insert(make_pair(j->first, j->second));
				}
				else
				{
					for (auto z = ListOfNotWeighted[j->first].begin(); z != ListOfNotWeighted[j->first].end(); z++)
					{
						if (z->first == i)
						{
							f = false;
							break;
						}
					}
					if (f)
					{
						ListOfNotWeighted[i].insert(make_pair(j->first, j->second));
					}
					f = true;
				}
			}
		}
	}
}

void GRAPH::writeGraph(string fileName)
{
	int countOfSpace = 0;
	outTestFile.open(fileName,ios::out);
	switch (typeOfGraf)
	{
	case 'C':
	{
				outTestFile << typeOfGraf << ' ' << N << endl;;
				outTestFile << R << ' ' << W << endl;
				for (int i = 0; i < N; i++)
				{
					for (int j = 0; j < N; j++)
					{
						if (countOfSpace != 0)
						{
							outTestFile << ' ';
						}
						outTestFile << matrixGraf[i][j];
						countOfSpace++;
					}
					countOfSpace = 0;
					outTestFile << endl;
				}
				break;
	}
	case 'L':
	{
				outTestFile << typeOfGraf<<' '<< N << endl;;
				outTestFile << R <<' '<< W << endl;
				if (W == true)
				{
					for (int i = 1; i < N + 1; i++)
					{
						for (auto j = adjListOfWeighted[i].begin(); j != adjListOfWeighted[i].end(); j++)
						{
							if (countOfSpace != 0)
							{
								outTestFile << ' ';
							}
							outTestFile << get<0>(*j) << ' ' << get<1>(*j);
						}
						outTestFile << endl;
						countOfSpace = 0;
					}
				}
				else
				{
					for (int i = 1; i < N + 1; i++)
					{
						for (auto j = adjListOfNotWeighted[i].begin(); j != adjListOfNotWeighted[i].end(); j++)
						{
							if (countOfSpace != 0)
							{
								outTestFile << ' ';
							}
							outTestFile << j->first;
							countOfSpace++;
						}
						outTestFile << endl;
						countOfSpace = 0;
					}
				}
				break;

	}
	case 'E':
	{
				outTestFile << typeOfGraf << ' ' << N << ' ' << M << endl;
				outTestFile << R << ' ' << W << endl;
				if (W == true)
				{
					for (int i = 1; i < N + 1; i++)
					{
						for (auto j = ListOfWeighted[i].begin(); j != ListOfWeighted[i].end(); j++)
						{
							outTestFile << i << ' ' << get<0>(*j) << ' ' << get<1>(*j) << endl;;
						}
						
					}
				}
				else
				{
					for (int i = 0; i < N + 1; i++)
					{
						for (auto j = ListOfNotWeighted[i].begin(); j != ListOfNotWeighted[i].end(); j++)
						{
							outTestFile << i << ' ' << j->first  << endl;
						}
					}
				}
				break;
	}
	}
}

void GRAPH::addEdge(int from, int to, int weight)
{
	switch (typeOfGraf)
	{
	case 'C':
	{
				matrixGraf[from-1][to-1] = weight;
				matrixGraf[to-1][from-1] = weight;
				break;
	}
	case 'L':
	{
				if (R == 1)
				{
					if (W == 1)
					{
						adjListOfWeighted[from].insert(make_tuple(to, weight, 1));
					}
					else
					{
						adjListOfNotWeighted[from].insert(make_pair(to, 1));
					}
				}
				else
				{
					if (W == 1)
					{
						adjListOfWeighted[from].insert(make_tuple(to, weight, 1));
						adjListOfWeighted[to].insert(make_tuple(from, weight, 1));

					}
					else
					{
						adjListOfNotWeighted[from].insert(make_pair(to, 1));
						adjListOfNotWeighted[to].insert(make_pair(from, 1));
					}
				}
	}
	case 'E':
	{
				if (R == 1)
				{
					if (W == 1)
					{
						ListOfWeighted[from].insert(make_tuple(to, weight, 1));
					}
					else
					{
						ListOfNotWeighted[from].insert(make_pair(to, 1));
					}
				}
				else
				{
					if (W == 1)
					{
						ListOfWeighted[from].insert(make_tuple(to, weight, 1));
						ListOfWeighted[to].insert(make_tuple(from, weight, 1));

					}
					else
					{
						ListOfNotWeighted[from].insert(make_pair(to, 1));
						ListOfNotWeighted[to].insert(make_pair(from, 1));
					}
				}
	}
	}
}

void GRAPH::removeEdge(int from, int to)
{
	switch (typeOfGraf)
	{
	case 'C':
	{
				matrixGraf[from - 1][to - 1] = 0;
				matrixGraf[to - 1][from - 1] = 0;
				break;
	}
	case 'L':
	{

				if (R == 1)
				{
					if (W == 1)
					{
						for (auto j = adjListOfWeighted[from].begin(); j != adjListOfWeighted[from].end(); j++)
						{
							if (get<0>(*j) == to)
							{
								adjListOfWeighted[from].erase(j);
							}
							break;
						}
					}
					else
					{
						for (auto j = adjListOfNotWeighted[from].begin(); j != adjListOfNotWeighted[from].end(); j++)
						{
							if (get<0>(*j) == to)
							{
								adjListOfNotWeighted[from].erase(j);
							}
							break;
						}						
					}
				}
				else
				{
					if (W == 1)
					{

						for (auto j = adjListOfWeighted[from].begin(); j != adjListOfWeighted[from].end(); j++)
						{
							if (get<0>(*j) == to)
							{
								adjListOfWeighted[from].erase(j);
							}
							break;
						}
						for (auto j = adjListOfWeighted[to].begin(); j != adjListOfWeighted[to].end(); j++)
						{
							if (get<0>(*j) == from)
							{
								adjListOfWeighted[to].erase(j);
							}
							break;
						}

					}
					else
					{
						for (auto j = adjListOfNotWeighted[from].begin(); j != adjListOfNotWeighted[from].end(); j++)
						{
							if (get<0>(*j) == to)
							{
								adjListOfNotWeighted[from].erase(j);
							}
							break;
						}
						for (auto j = adjListOfNotWeighted[to].begin(); j != adjListOfNotWeighted[to].end(); j++)
						{
							if (get<0>(*j) == from)
							{
								adjListOfNotWeighted[to].erase(j);
							}
							break;
						}
					}
				}
				break;
	}
	case 'E':
	{
				if (R == 1)
				{
					if (W == 1)
					{
						for (auto j = ListOfWeighted[from].begin(); j != ListOfWeighted[from].end(); j++)
						{
							if (get<0>(*j) == to)
							{
								ListOfWeighted[from].erase(j);
							}
							break;
						}
					}
					else
					{
						for (auto j = ListOfNotWeighted[from].begin(); j != ListOfNotWeighted[from].end(); j++)
						{
							if (get<0>(*j) == to)
							{
								ListOfNotWeighted[from].erase(j);
							}
							break;
						}
					}
				}
				else
				{
					if (W == 1)
					{

						for (auto j = ListOfWeighted[from].begin(); j != ListOfWeighted[from].end(); j++)
						{
							if (get<0>(*j) == to)
							{
								ListOfWeighted[from].erase(j);
							}
							break;
						}
						for (auto j = ListOfWeighted[to].begin(); j != ListOfWeighted[to].end(); j++)
						{
							if (get<0>(*j) == from)
							{
								ListOfWeighted[to].erase(j);
							}
							break;
						}

					}
					else
					{
						for (auto j = ListOfNotWeighted[from].begin(); j != ListOfNotWeighted[from].end(); j++)
						{
							if (get<0>(*j) == to)
							{
								ListOfNotWeighted[from].erase(j);
							}
							break;
						}
						for (auto j = ListOfNotWeighted[to].begin(); j != ListOfNotWeighted[to].end(); j++)
						{
							if (get<0>(*j) == from)
							{
								ListOfNotWeighted[to].erase(j);
							}
							break;
						}
					}
				}
				break;
	}
	}
}

int GRAPH::changeEdge(int from, int to, int newWeight)
{
	int oldInt;
	int ID;
	M++;
	switch (typeOfGraf)
	{
	case 'C':
	{
				oldInt = matrixGraf[from - 1][to - 1];
				matrixGraf[from - 1][to - 1] = newWeight;
				matrixGraf[to - 1][from - 1] = newWeight;
				return oldInt;
				break;
	}
	case 'L':
	{

				if (R == 1)
				{

					for (auto j = adjListOfWeighted[from].begin(); j != adjListOfWeighted[from].end(); j++)
					{
						if (get<0>(*j) == to)
						{
							oldInt = get<0>(*j);
							ID = get<1>(*j);
							adjListOfWeighted[from].erase(j);
							adjListOfWeighted[from].insert(make_tuple(to, newWeight, ID));
						}
						break;
					}

				}
				else
				{


					for (auto j = adjListOfWeighted[from].begin(); j != adjListOfWeighted[from].end(); j++)
					{
						if (get<0>(*j) == to)
						{
							oldInt = get<0>(*j);
							ID = get<1>(*j);
							adjListOfWeighted[from].erase(j);
							adjListOfWeighted[from].insert(make_tuple(to, newWeight, ID));
						}
						break;
					}
					for (auto j = adjListOfWeighted[to].begin(); j != adjListOfWeighted[to].end(); j++)
					{
						if (get<0>(*j) == from)
						{
							oldInt = get<0>(*j);
							ID = get<1>(*j);
							adjListOfWeighted[to].erase(j);
							adjListOfWeighted[to].insert(make_tuple(from, newWeight, ID));
						}
						break;
					}


				}
				return oldInt;
				break;
	}
	case 'E':
	{
				if (R == 1)
				{

					for (auto j = ListOfWeighted[from].begin(); j != ListOfWeighted[from].end(); j++)
					{
						if (get<0>(*j) == to)
						{
							oldInt = get<0>(*j);
							ID = get<1>(*j);
							ListOfWeighted[from].erase(j);
							ListOfWeighted[from].insert(make_tuple(to, newWeight, ID));
						}
						break;
					}

				}
				else
				{


					for (auto j = ListOfWeighted[from].begin(); j != ListOfWeighted[from].end(); j++)
					{
						if (get<0>(*j) == to)
						{
							oldInt = get<0>(*j);
							ID = get<1>(*j);
							ListOfWeighted[from].erase(j);
							ListOfWeighted[from].insert(make_tuple(to, newWeight, ID));
						}
						break;
					}
					for (auto j = ListOfWeighted[to].begin(); j != ListOfWeighted[to].end(); j++)
					{
						if (get<0>(*j) == from)
						{
							oldInt = get<0>(*j);
							ID = get<1>(*j);
							ListOfWeighted[to].erase(j);
							ListOfWeighted[to].insert(make_tuple(from, newWeight, ID));
						}
						break;
					}


				}
				return oldInt;
				break;
	}
	}
}